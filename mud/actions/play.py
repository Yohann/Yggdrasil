from .action import Action2, Action3
from mud.events import PlayEvent


class PlayAction(Action2):
    EVENT = PlayEvent
    RESOLVE_OBJECT = "resolve_for_use"
    ACTION = "play"
